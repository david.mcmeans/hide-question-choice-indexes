# Hide question choice indexes

Hide the index values used to sort your question choices. Useful when the normal sort for the choices is alphabetical and you want to enforce a particular order.

Although any index scheme could be used, the assumption here is two digits followed by a period followed by a space.

Ex.

2.4.1 What kind of light saber to you prefer?

[ ] 01. Green Annakin Skywalker model\
[ ] 02. Blue Yoda model\
[ ] 03. Red dual-blade Sith lord model\
[ ] 99. Other

When the form building software doesn't let you specify the order of the choices to your question, but instead always sorts alphabetically, you can order them using an indexing scheme as shown.

However, the index is not meaningful to the choice necessarily, and it might be preferrable to hide it.

So long as your form building software supports javascript, the index can be hidden using javascript.

## Remove the Indexes

In the following example, the path to the question choices is `.MultiChoicePickListQuestion .checkboxLI label`. Obviously, your form question will have a different selector.

```javascript
var myRegex = /^\d{1,2}\. /;
jQuery('.MultiChoicePickListQuestion .checkboxLI label').each(function(i,e) {
  var o = jQuery(this);
  var t = o.text().replace(myRegex, '');
  o.text(t);
});
```

## Wrap the Indexes

A better approach might be to wrap the indexes so you can hide them using CSS. This requires, of course, your form building software permits you to include CSS.

```javascript
var myRegex = /^(\d{1,3}\. )/;
jQuery('.MultiChoicePickListQuestion .checkboxLI label').each(function(i,e) {
  var o = jQuery(this);
  var t = o.text().replace(myRegex, '<span class="iui-answer-index">$1</span> ');
  o.html(t);
});
```

Then, a CSS rule could be added to hide the indices:

```css
.iui-answer-index {
    display: none;
}
```

Hiding the index will likely result in the label being too close to the checkbox however. Give it some padding.

Ex.

```css
.MultiChoicePickListQuestion .checkboxLI label {
    padding-left: 10px;
}
```

The real benefit of using the wrapper, of course, is the ability to style the index. Perhaps you want to use a smaller font and a diminished color.

```css
.iui-answer-index {
    color: hsla(47, 31%, 42%, 1);
    font-size: x-small;
}
```
![Diminished answer indices](images/image-1.png "Diminished answer indices")

## When you have textnodes instead of html elements

What do you do when your question answers are simple textnodes instead of html elements?

For example, your markup might look like this:

```html
<div class="PickListQuestion NotHidden FormInput" style="display: block;">
    <img src="/pic/icon/NotCheckedRadio.gif">
    01. Request to continue obtaining consent for study with currently approved consent(s). A clean copy of the approved form(s) is attached to this submission for review and stamping.
    <br>
    <img src="/pic/icon/NotCheckedRadio.gif">
    02. Study does not involve written consent forms.
    <br>
    <img src="/pic/icon/NotCheckedRadio.gif">
    03. Enrollment has ended, no consent renewal needed.
    <br>
    <img src="/pic/icon/NotCheckedRadio.gif">
    04. Requesting amendment to consent forms as indicated in Question 8.0 Modification.
    <br>
</div>
```
You cannot locate these textnodes using CSS selectors. You must first wrap them with spans.

### Add a filter to find text nodes

First, you'll need a way to locate textnodes. Add this function to your form.

```javascript
function isTextNode() { 
    return this.nodeType == Node.TEXT_NODE && this.nodeValue.trim().length != 0; 
}
```

### Wrap textnodes with spans

Then, wrap them with spans. Here's how you might do it using jQuery. 

Note: If your textnodes are not wrapped, trying selecting the immediate parent of the textnodes. In the example markup above, this would be `.PickListQuestion`.

```javascript
// wrap text nodes with spans
jQuery('[elementid^="selrbQ"] .PickListQuestion.NotHidden').contents().filter(isTextNode).wrap(jQuery('<span />', {class:'iui-text'}));
```
We will tag our spans with the class `iui-text`. This will produce

```html
<div class="PickListQuestion NotHidden FormInput" style="display: block;">
    <img src="/pic/icon/NotCheckedRadio.gif">
    <span class="iui-text">01. Request to continue obtaining consent for study with currently approved consent(s). A clean copy of the approved form(s) is attached to this submission for review and stamping.</span>
    <br>
```
### Include wrapped text nodes in indices search  

Now, you can include your wrapped textnodes in your search for answer indices.

```javascript
var ansIndexRegex = /^(\d{1,3}\. )/;
jQuery('.MultiChoicePickListQuestion .checkboxLI label, [elementid^="selrbQ"] .PickListQuestion.NotHidden .iui-text').each(function(i,e) {
    var o = jQuery(this);
    var t = o.text().replace(ansIndexRegex, '<span class="iui-answer-index">$1</span> ');
    o.html(t);
});
```
And you will get

```html
<div class="PickListQuestion NotHidden FormInput" style="display: block;">
    <img src="/pic/icon/NotCheckedRadio.gif">
    <span class="iui-text"><span class="iui-answer-index">01. </span> Request to continue obtaining consent for study with currently approved consent(s). A clean copy of the approved form(s) is attached to this submission for review and stamping.</span>
    <br>
```
